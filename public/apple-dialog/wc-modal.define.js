const template = document.createElement('template');
template.innerHTML = `
  <dialog>
    <slot></slot>
  </dialog>
`;

export class WcModal extends HTMLElement {
  constructor() {
    super();
		this.attachShadow({ mode: 'open' });
		this.shadowRoot.appendChild(template.content.cloneNode(true));
    this._dialogNode = this.shadowRoot.querySelector('dialog');
  }


  set modal(value) {
    if (value) {
      this.setAttribute('modal', '')
    } else {
      this.removeAttribute('modal');
    }
  }

  get modal() {
    return this.hasAttribute('modal');
  }

  set open(value) {
    if (!this._dialogNode) { return; }
    if (this._dialogNode.open && value) { return; }

    const method = value ? (this.modal ? 'showModal' : 'show') : 'close';
    console.log('dialog ' + method);
    this._dialogNode[method](); 
  }

  get open() {
    return !!this._dialogNode?.open;
  }

}
customElements.define('wc-modal', WcModal);
