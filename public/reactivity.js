const context = [];

function subscribe(running, subscriptions) {
	subscriptions.add(running);
	running.dependencies.add(subscriptions);
}

function createSignal(value) {
	const subscriptions = new Set();

	const read = () => {
		const running = context[context.length - 1];
		if (running) subscribe(running, subscriptions);
		return value;
	}
	const write = (nextValue) => { 
		value = nextValue; 

		subscriptions.forEach(sub => sub.execute());
		// for (const sub of [...subscriptions]) {
		// 	sub.execute();
		// }

	}
	
	return [read, write];
}

const [count, setCount] = createSignal(3);

console.log('count (1)', count());

setCount(5);
console.log('count (2)', count());
  
setCount(count() * 2);
console.log('count (3)', count());

/*
	question: context mentioned, but where is this happening? Mabe next chapter

	https://dev.to/ryansolid/building-a-reactive-library-from-scratch-1i0p
	Bookmark: Reactions and Derivations
 */
